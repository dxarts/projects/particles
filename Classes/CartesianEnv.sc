CartesianEnv {
	var <>levels, <>times, <>curves;
	var <>x, <>y, <>z;

	*new { |levels = ([Cartesian.new,Cartesian.new]), times = ([1]), curves = 'lin'|
		^super.newCopyArgs(levels, times, curves).init
	}

	init {
		x = Env.new(levels.collect(_.x), times, curves);
		y = Env.new(levels.collect(_.y), times, curves);
		z = Env.new(levels.collect(_.z), times, curves);
	}

	at { |time|
		^Cartesian.new(x[time], y[time], z[time])
	}

	scale { |levelScalar = 1, timeScalar = 1|
		this.levels = this.levels.collect{ |cart| cart * levelScalar.asCartesian };
		this.times = times * timeScalar;
		this.init
	}

	translate { |cartesian|
		this.levels = this.levels.collect{ |cart| cart + cartesian.asCartesian };
		this.init
	}

	translateEnv { |aCartesianEnv|
		this.x.levels = this.x.levels.collect{ |level, i|
			(i == 0).if({
				level + aCartesianEnv.x[0]
			}, {
				level + aCartesianEnv.x[times[0..i-1].sum]
			})
		};
		this.y.levels = this.y.levels.collect{ |level, i|
			(i == 0).if({
				level + aCartesianEnv.y[0]
			}, {
				level + aCartesianEnv.y[times[0..i-1].sum]
			})
		};
		this.z.levels = this.z.levels.collect{ |level, i|
			(i == 0).if({
				level + aCartesianEnv.z[0]
			}, {
				level + aCartesianEnv.z[times[0..i-1].sum]
			})
		};
	}

	blend { |aCartesianEnv|
		this.x = this.x.blend(aCartesianEnv.x);
		this.y = this.y.blend(aCartesianEnv.y);
		this.z = this.z.blend(aCartesianEnv.z)
	}

}
