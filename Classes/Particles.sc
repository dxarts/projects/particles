Particle {
	var <>location, <>velocity, <>lifeSpan, <>id, <>acceleration;

	*new { |location = (Cartesian.new), velocity = (Cartesian.new), lifeSpan = 1, id = 0, acceleration = (Cartesian.new)|
		^super.newCopyArgs(location, velocity, lifeSpan, id, acceleration).init;
	}

	init {
		location = location.asCartesian;
		velocity = velocity.asCartesian;
	}

	update { |newLife = -0.02|
		// update the postition
		velocity = velocity + acceleration;
		location = location + velocity;
		lifeSpan = lifeSpan + newLife
	}

	setVel { |rho| velocity = velocity.asSpherical.rho_(rho).asCartesian }

}

Particles {
	var <>particles, <>lifeRate, <runTask, <>velocity;
	var <>runFunc, <>particleFunc, <forces, <>frameRate = 0.08;
	var flockParticleFunc, flocking = false, nearParticles;
	var <>maxspeed = 0.03, <>maxforce = 0.01, <>attraction = 1.0, <>repulsion = 0.1, <>centerLocation, <>centerForce = 0.5;

	*new { |particles = ([]), lifeRate = -0.01|
		^super.newCopyArgs(particles, lifeRate).init
	}

	init {
		this.removeAllFuncs;
		forces = Forces.new(this);
		centerLocation = Cartesian.new;
		nearParticles = Array.new;
		particles.isKindOf(SimpleNumber).if({
			particles = particles.collect{
				Particle.new(Cartesian.new, Cartesian.new(0.1.rand2, 0.1.rand2, 0.1.rand2))
			}
		})
	}

	removeAllFuncs {
		this.runFunc_(FunctionList.new);
		this.particleFunc_(FunctionList.new);
	}

	addRunFunc { |func| this.addFuncTo('runFunc', func) }
	addParticleFunc { |func| this.addFuncTo('particleFunc', func) }
	removeRunFunc { |func| this.removeFuncFrom('runFunc', func) }
	removeParticleFunc { |func| this.removeFuncFrom('particleFunc', func) }

	addParticle { |particle| particles = particles.add(particle) }
	removeParticle { |particle| particles.remove(particle) }
	getCenter { |selector = 'location'| ^this.sum(selector)/this.size }
	size { ^particles.size }
	removeAllParticles { particles = [] }

	sum { |selector = 'location'|
		var cart = Cartesian.new;
		particles.do{ |particle|
			cart = cart + particle.perform(selector);
		};
		^cart
	}

	kdTree { ^KDTree(this.particles.collect{ |particle, i| particle.location.asArray ++ [i] }, lastIsLabel: true) }

	getParticlesByDistance { |particle, distance = 30|
		var dist;
		^Particles.new(this.particles.select{ |thisParticle| dist = thisParticle.location.dist(particle.location); (dist > 0) and: (dist <= distance) })
	}

	getParticlesByDistanceKD { |particle, distance = 30, kdtree|
		var index, indices;
		index = this.particles.detectIndex{ |thisParticle| thisParticle == particle };
		indices = kdtree.radiusSearch(particle.location.asArray, distance).collect(_.label);
		indices.remove(index);
		^Particles.new(this.particles[indices])
	}

	getNearest { |particle|
		var dists, index;
		dists = particles.collect{ |thisParticle| thisParticle.location.dist(particle.location) };
		index = dists.minIndex;
		^[particles[index], dists[index]]
	}

	run {
		var now = 0.0;
		runTask = Task({

			inf.do{
				(nearParticles.size != (this.size - 1)).if( nearParticles = Array.new(this.size - 1));
				runFunc.(now);
				particles.do{ |particle|
					particleFunc.(particle, now);
					// velocity !? { particle.setVel(velocity) };
					particle.update(lifeRate);
					(particle.lifeSpan < 0.0).if({ this.removeParticle(particle) })
				};
				now = now + frameRate[now];
				frameRate[now].wait
			}
		}).play
	}

	flockVel { |particle, now|
		var velocity, dist, nearParticles = [], repulse, diff, inc = 0, cLocation;
		repulse = Cartesian.new;
		this.particles.do { |thisParticle|
			dist = thisParticle.location.dist(particle.location);
			(dist > 0).if({
				(dist <= this.attraction[now]).if({
					nearParticles = nearParticles.add(thisParticle)
				});
				(dist <= this.repulsion[now]).if({
					// Calculate vector pointing away from neighbor
					diff = particle.location - thisParticle.location;
					diff = diff.normalize;
					diff = diff/dist;        // Weight by distance
					repulse = repulse + diff;
					inc = inc + 1;
				})

			})
		};

		(inc > 0).if({ repulse = repulse/inc });
		(repulse.rho > 0).if({
			// Implement Reynolds: Steering = Desired - Velocity
			repulse = repulse.normalize;
			repulse = repulse * this.maxspeed[now];
			repulse = repulse - particle.velocity;
			repulse = repulse.limit(this.maxforce[now]);
		});

		cLocation = this.centerLocation.isKindOf(CartesianEnv).if({ this.centerLocation[now] }, { this.centerLocation });

		^((repulse * 1.5) + this.forces.align2(particle, nearParticles, this.maxspeed[now], this.maxforce[now]) +
			this.forces.attract2(particle, nearParticles, this.maxspeed[now], this.maxforce[now]) +
			this.forces.seek(particle, cLocation, this.maxspeed[now], this.centerForce[now] * particle.location.rho * this.maxforce[now]))

	}

	// test new algo
	flockVel2 { |particle, now|
		var velocity, dist, repulse, diff, repSize = 0, attSize = 0, cLocation;
		repulse = Cartesian.new;
		this.particles.do { |thisParticle|
			dist = thisParticle.location.dist(particle.location);
			(dist > 0).if({
				(dist <= this.attraction[now]).if({
					nearParticles = nearParticles.add(thisParticle);
					attSize = attSize + 1;
				});
				(dist <= this.repulsion[now]).if({
					// Calculate vector pointing away from neighbor
					diff = particle.location - thisParticle.location;
					diff = diff.normalize;
					diff = diff/dist;        // Weight by distance
					repulse = repulse + diff;
					repSize = repSize + 1;
				})
			})
		};

		(repSize > 0).if({ repulse = repulse/repSize });
		(repulse.rho > 0).if({
			// Implement Reynolds: Steering = Desired - Velocity
			repulse = repulse.normalize;
			repulse = repulse * this.maxspeed[now];
			repulse = repulse - particle.velocity;
			repulse = repulse.limit(this.maxforce[now]);
		});

		cLocation = this.centerLocation.isKindOf(CartesianEnv).if({ this.centerLocation[now] }, { this.centerLocation });

		^((repulse * 1.5) + this.forces.align2(particle, nearParticles[..attSize], this.maxspeed[now], this.maxforce[now]) +
			this.forces.attract2(particle, nearParticles[..attSize], this.maxspeed[now], this.maxforce[now]) +
			this.forces.seek(particle, cLocation, this.maxspeed[now], this.centerForce[now] * particle.location.rho * this.maxforce[now]))

	}

	flock { |fRate, mspeed, mforce, attraction, repulsion, cLocation, cForce|
		fRate !? { this.frameRate_(fRate) };
		cForce !? { this.centerForce_(cForce) };
		cLocation !? { this.centerLocation_(cLocation) };
		mforce !? { this.maxforce_(mforce) };
		mspeed !? { this.maxspeed_(mspeed) };
		repulsion !? { this.repulsion_(repulsion) };
		attraction !? { this.attraction_(attraction) };

		flockParticleFunc = { |particle, now| particle.velocity_(particle.velocity + this.flockVel(particle, now)) };

		this.addParticleFunc(flockParticleFunc);
		flocking = true;
		this.run
	}

	pause { runTask.pause }
	resume { runTask.resume }
	stop { runTask.stop; flocking.if({ this.removeParticleFunc(flockParticleFunc); flocking = false }) }

	particleEnvs { |positions, times, scale, translate|
		var envs, envScale;

		envs = positions.collect{ |position, i| CartesianEnv.new(position, times) };

		envScale = envs.collect{ |env|
			[env.x.levels.abs.maxItem, env.y.levels.abs.maxItem, env.z.levels.abs.maxItem].maxItem
		}.maxItem;

		scale.notNil.if({
			envs.do{ |env|
				scale.isKindOf(CartesianEnv).if({
					env.scale(envScale.reciprocal).blend(scale)
				}, {
					env.scale(envScale.reciprocal).scale(scale)
				})
			};
		});

		translate.notNil.if({
			envs.do{ |env|
				translate.isKindOf(CartesianEnv).if({
					env.translateEnv(translate)
				}, {
					env.translate(translate)
				})
			}
		});

		^envs

	}

	nrt { |frameRate, duration|
		var now = 0.0;
		frameRate.isKindOf(Env).if({ duration = frameRate.times.sum });
		while({ now < duration }, {
			runFunc.(now);
			this.getCenter.postln;
			particles.do{ |particle|
				particleFunc.(particle, now);
				particle.update(lifeRate);
			};
			now = now + frameRate[now];
		});

	}

	flockNRT { |frameRate, duration, mspeed, mforce, attraction, repulsion, cLocation, cForce|
		cForce !? { this.centerForce_(cForce) };
		cLocation !? { this.centerLocation_(cLocation) };
		mforce !? { this.maxforce_(mforce) };
		mspeed !? { this.maxspeed_(mspeed) };
		repulsion !? { this.repulsion_(repulsion) };
		attraction !? { this.attraction_(attraction) };

		flockParticleFunc = { |particle, now| particle.velocity_(particle.velocity + this.flockVel(particle, now)) };

		this.addParticleFunc(flockParticleFunc);
		this.nrt(frameRate, duration);
		this.removeParticleFunc(flockParticleFunc)
	}

	envs { |frameRate, scale, translate, duration|
		var now = 0.0, positions, times;
		positions = Array.newClear(particles.size);
		frameRate = frameRate ?? { this.frameRate };
		frameRate.isKindOf(Env).if({ duration = frameRate.times.sum });
		while({ now < duration }, {
			runFunc.(now);
			particles.do{ |particle,  i|
				particleFunc.(particle, now);
				particle.update(lifeRate);
				positions[i] = positions[i].add(particle.location)
			};
			times = times.add(frameRate[now]);
			now = now + frameRate[now];
		});

		^this.particleEnvs(positions, times, scale, translate)
	}

	flockEnvs { |frameRate, scale, translate, mspeed, mforce, attraction, repulsion, cLocation, cForce, duration|
		var envs;
		cForce !? { this.centerForce_(cForce) };
		cLocation !? { this.centerLocation_(cLocation) };
		mforce !? { this.maxforce_(mforce) };
		mspeed !? { this.maxspeed_(mspeed) };
		repulsion !? { this.repulsion_(repulsion) };
		attraction !? { this.attraction_(attraction) };

		flockParticleFunc = { |particle, now| particle.velocity_(particle.velocity + this.flockVel(particle, now)) };

		this.addParticleFunc(flockParticleFunc);
		envs = this.envs(frameRate, scale, translate, duration);
		this.removeParticleFunc(flockParticleFunc);
		^envs

	}

	plot { |envs|
		var pointView, window, drawTask, now = 0.0, carts, max;
		max = envs.collect{ |env| Cartesian.new(env.x.levels.abs.maxItem, env.y.levels.abs.maxItem, env.z.levels.abs.maxItem) };
		max = max[max.collect(_.rho).maxIndex];
		carts = envs.size.collect{ Cartesian.new } ++ max;
		window = Window.new("Particles", Window.screenBounds);
		pointView = PointView.new(window, window.bounds).showIndices_(false).rotate_(0).tumble_(0.5pi).pointColors_(Color.black);
		drawTask = Task({
			loop{
				envs.do{ |env, i|
					carts[i].x_(env.x[now]);
					carts[i].y_(env.y[now]);
					carts[i].z_(env.z[now])
				};
				{pointView.points_(carts ++ max)}.defer;
				now = now + 0.01;
				0.01.wait;
			}
		}).play;
		window.front;
		window.onClose_({ drawTask.stop });
	}

	plotRT {
		var pointView, window, drawTask, now = 0.0, carts, max;
		window = Window.new("Particles", Window.screenBounds);
		pointView = PointView.new(window, window.bounds).showIndices_(false).rotate_(0).tumble_(0.5pi).pointColors_(Color.black);

		this.flock;
		drawTask = Task({
			loop{
				{pointView.points_(this.particles.collect(_.location))}.defer;
				now = now + 0.01;
				0.01.wait;
			}
		}).play;
		window.front;
		window.onClose_({ drawTask.stop });
	}

}

Forces {
	var <>particles;

	*new { |particles|
		^super.newCopyArgs(particles)
	}

	bounce { |particle| particle.velocity_(particle.velocity.asSpherical.rho_(particle.velocity.rho * -1).asCartesian) }

	bounceAll { |particle, radius = 1|
		var thisParticle, dist;
		#thisParticle, dist = particles.getNearest(particle);
		(dist < radius).if({ this.bounce(particle) })
	}

	// return normalized vector from particle to target
	seek { |particle, target, maxspeed = 1, maxforce = 0.02|
		var desired, steer;
		desired = target - particle.location;  // A vector pointing from the location to the target
		// Normalize desired and scale to maximum speed
		desired = desired.normalize;
		desired = desired * maxspeed;
		// Steering = Desired minus Velocity
		steer = desired - particle.velocity;
		steer = steer.limit(maxforce);  // Limit to maximum steering force
		^steer;
	}

	// Separation
	// Method checks for nearby particles and steers away
	repulse { |particle, repulsion, maxspeed, maxforce|
		var cart, diff, inc = 0;
		cart = Cartesian.new;
		// For every particle in the system, check if it's too close
		particles.particles.do { |thisParticle|
			var dist = particle.location.dist(thisParticle.location);
			((dist > 0) and: (dist <= repulsion)).if({
				// Calculate vector pointing away from neighbor
				diff = particle.location - thisParticle.location;
				diff = diff.normalize;
				diff = diff/dist;        // Weight by distance
				cart = cart + diff;
				inc = inc + 1
			})
		};

		(inc > 0).if({ cart = cart/inc });

		// As long as the vector is greater than 0
		(cart.rho > 0).if({
			// Implement Reynolds: Steering = Desired - Velocity
			cart = cart.normalize;
			cart = cart * maxspeed;
			cart = cart - particle.velocity;
			cart = cart.limit(maxforce);
		}, {
			cart
		})
		^cart
	}

	// Alignment
	// For every nearby particle in the system, calculate the average velocity
	align { |particle, attraction, maxspeed, maxforce|
		var cart, theseParticles;
		// select only particles that are close enough
		theseParticles = particles.getParticlesByDistance(particle, attraction);
		^theseParticles.particles.notEmpty.if({
			cart = theseParticles.getCenter('velocity');
			cart = cart.normalize;
			cart = cart * maxspeed;
			cart = cart - particle.velocity;
			cart = cart.limit(maxforce);
		}, {
			Cartesian.new;
		})
	}

	align2 { |particle, particles, maxspeed, maxforce|
		var vel;
		^particles.notEmpty.if({
			vel = particles.collect{ |thisParticle| thisParticle.velocity.asArray }.sum.asCartesian/particles.size;
			vel = vel.normalize;
			vel = vel * maxspeed;
			vel = vel - particle.velocity;
			vel = vel.limit(maxforce);
		}, {
			Cartesian.new;
		})
	}



	// Cohesion
	// For the average location (i.e. center) of all nearby particles, calculate steering vector towards that location
	attract { |particle, attraction, maxspeed, maxforce|
		var theseParticles;
		// select only particles that are close enough
		theseParticles = particles.getParticlesByDistance(particle, attraction);
		^theseParticles.particles.notEmpty.if({
			this.seek(particle, theseParticles.getCenter, maxspeed, maxforce)
		}, {
			Cartesian.new;
		})
	}

	attract2 { |particle, particles, maxspeed, maxforce|
		^particles.notEmpty.if({
			this.seek(particle, particles.collect{ |thisParticle| thisParticle.location.asArray }.sum.asCartesian/particles.size, maxspeed, maxforce)
		}, {
			Cartesian.new;
		})
	}

}